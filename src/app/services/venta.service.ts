import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment } from 'src/environments/environment';
import { Venta } from '../components/pages/models/venta.model';


const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})

export class VentaService {

  constructor(private http: HttpClient) { }



  guardarVenta(venta: Venta) {
    return this.http.post(`${base_url}/nuevaVenta`,venta);
  }
}
