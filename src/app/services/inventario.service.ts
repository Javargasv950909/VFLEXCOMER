import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment } from 'src/environments/environment';
import { Producto } from '../components/pages/models/producto.model';
import { Observable } from 'rxjs';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class InventarioService {

  constructor(private http: HttpClient) { }

  listaInventario():Observable<any> {
      return this.http.get(`${base_url}/productos`);
  }

  crearProducto(producto: Producto) {
    console.log("entre a insertar el prod: "+producto.id);
    return this.http.post(`${base_url}/nuevoProducto`,producto);
  }

  eliminarProducto(id:string){
    console.log("entre a eliminar el prod: "+id);
    this.http.delete(`${base_url}/eliminarProducto/${id}`)
      .subscribe(data => {
        console.log("esto imprime el eliminar: ",data)
      })
  }

  actualizarProducto(id: string, producto:Producto){
    console.log("entre a actualizar el prod: "+producto.id);
    this.http.put(`${base_url}/actualizarProducto/${id}`,producto)
    .subscribe(data => {
      console.log(data);
    })
  }

}
