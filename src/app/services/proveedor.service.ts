import { Injectable } from '@angular/core';
import {environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Proveedor } from '../components/pages/models/proveedor.model';
import { Observable } from 'rxjs';


const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})

export class ProveedorService {

  

  constructor(private http: HttpClient) { }

  listaProveedores():Observable<any> {
    return this.http.get(`${base_url}/proveedores`);
  }

  crearProveedor(proveedor: Proveedor) {
    return this.http.post(`${base_url}/nuevoProveedor`,proveedor);
  }
}
