import { Component } from '@angular/core';
import { MessagesAppService } from './shared/messages-app/messages-app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PuntoVentas';

  constructor(private messageService: MessagesAppService) {
    this.messageService.loadAppMessages();
  }
}
