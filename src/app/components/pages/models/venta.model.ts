export class Venta {
    id?: string;
    id_tienda: string;
    producto: any;
    cantidad: number;
    precio: number;
    productos_detalle: Array<any> = [];
    precio_total_venta: number = 0;
}