import { Producto } from './producto.model';
export class Compra {
    id_tienda: string;
    fecha_compra: Date;
    proveedor: string;
    productos: Producto[];
    cantidad: number;
    precio_compra: number;
}