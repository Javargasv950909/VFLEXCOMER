export class Producto {
    id?: string;
    bc?: string;
    nombre: string;
    id_tienda: string;
    cantidad: number;
    precio: number;
}