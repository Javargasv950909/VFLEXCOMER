export class Proveedor {
    id?: string;
    nombre_proveedor: string;
    id_tienda: string;
    contacto_proveedor: string;
    dia_promedio: string;
}