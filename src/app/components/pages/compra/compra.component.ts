import { Component, OnInit,ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { InventarioService } from '../../../services/inventario.service';
import { Producto } from '../models/producto.model';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Proveedor } from '../models/proveedor.model';
import { Compra } from '../models/compra.model';
import { ProveedorService } from '../../../services/proveedor.service';
import Swal from 'sweetalert2';
import { MessagesAppService } from '../../../shared/messages-app/messages-app.service';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html',
  styleUrls: ['./compra.component.scss']
})
export class CompraComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('productoselec') productoSelect: Producto;

  model: NgbDateStruct;
  loader: boolean = true;
  fecha_compra = new Date();
  public isCollapsed = false;
  productos: Producto[] = [];
  proveedor: Proveedor = new Proveedor();
  proveedores: Proveedor[] = [];
  listaproveedores: MatTableDataSource<Proveedor>;
  compra: Compra = new Compra()

  
  constructor(private modalService: NgbModal, private inventarioService: InventarioService, private proveedorService: ProveedorService, public messageService: MessagesAppService) {
    this.cargarProveedores();
    this.cargarFormularioCompra();
    this.cargarProductos();
  }
  
  
    ngOnInit(): void {
  }

  // CARGAR PROVEEDORES
  cargarProveedores() {
    this.proveedorService.listaProveedores()
      .subscribe((data:any) => {
        this.listaproveedores = new MatTableDataSource<Proveedor>(data);
        this.listaproveedores.paginator = this.paginator;
        this.listaproveedores.sort = this.sort;
        this.proveedores = data;
      })
  }

  // LIMPIAR CAMPOS PROVEEDOR

  clearProveedor(){
    this.proveedor.nombre_proveedor = '';
    this.proveedor.contacto_proveedor = '';
  }
  

  // CODIGO PARA GENERAR COLUMNAS Y DATA EN LA TABLA Y FILTRO PROVEEDORES/////
  displayedColumns: string[] = ['nombre_proveedor', 'contacto_proveedor', 'fecha_proveedor'];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.listaproveedores.filter = filterValue.trim().toLowerCase();
  }

 // METODO PARA CARGAR PRODUCTOS AL SELECT
 cargarProductos() {
  this.inventarioService.listaInventario()
  .subscribe(data => {
    console.log(data.productos);
    this.productos = data.productos;
  });
 }

 cargarFormularioCompra() {
   console.log(this.fecha_compra);
 }

  // METODO PARA ABRIR POP UP LISTAR PROVEEDORES
 openProveedores(content:any) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  // METODO PARA ABRIR POP UP INFORMACION DIA PROMEDIO
 openDpromedio(content:any) {
  this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  // METODO PARA CREAR NUEVOS PROVEEDORES 
  crearProveedor(form: NgForm) {

    if(form.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer te falta un campo. Intenta nuevamente',
      })
    }
    
    this.proveedor.id_tienda = "123456789";
    this.proveedorService.crearProveedor(this.proveedor)
      .subscribe(resp => {
        console.log("se realizo la insersion"+this.proveedor.id);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Proveedor guardado exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
        this.clearProveedor();
      })
  }

  nuevoProducto() {
    const nuevoProducto: Producto = {
      nombre: '',
      id_tienda: '3243242',
      cantidad: 0,
      precio: 0
    }
    this.compra.productos.push(nuevoProducto);
  }

  eliminarListaProducto(i: number) {
    
  }


  ///METODO PARA GUARDAR COMPRA
  guardarCompra() {

  }

}
