import { Component, OnInit } from '@angular/core';
import { InventarioService } from '../../../services/inventario.service';
import { Producto } from '../models/producto.model';
import { Venta } from '../models/venta.model';
import { VentaService } from '../../../services/venta.service';
import Swal from 'sweetalert2';
import { UtilService } from '../../../shared/util.service';
import { ConstantsVflex } from '../../../shared/constants/constants';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss']
})
export class VentaComponent implements OnInit {


  fecha_compra = new Date();
  productosIniciales:  Producto[] = [];
  venta: Venta = new Venta();
  ventasAlmacenadas = [];

  constructor(private inventarioService: InventarioService, private ventaService: VentaService, private Util: UtilService) { 
    this.venta.cantidad = 0;
  }

  ngOnInit(): void {
    this.listarProductos();
  }
  
  // Metodo para listar los productos para la venta
  listarProductos() {
    this.inventarioService.listaInventario()
      .subscribe( data => {
        this.productosIniciales = data.productos;
      })
  }

  // Metodo change para traer el precio y la cantidad del producto seleccionado
  precioCantidad() {
    let data = this.venta.producto.split("-");
    this.venta.precio = Number(data[1]);
  }

  // Metodo para agregar productos al array de la venta
  agregarProducto() {
    console.log("este es el producto", this.venta.producto);
    if(this.venta.producto == '' || this.venta.producto == undefined) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No has seleccionado ningun producto',
      })
    } else if(this.venta.cantidad == 0){
       {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'La cantidad del producto debe ser mayor a 0',
        })
      }
    } else {
      let data = this.venta.producto.split("-");
      let existe = this.venta.productos_detalle.findIndex(e => e.producto_id === data[0]);
  
      if(existe != -1) {
        this.venta.productos_detalle[existe] = {
          producto_id: data[0],
          producto_nombre: data[2],
          cantidad:  Number(this.venta.productos_detalle[existe].cantidad) + Number(this.venta.cantidad),
          precio: this.venta.precio,
          subtotal: (Number(this.venta.productos_detalle[existe].cantidad) + Number(this.venta.cantidad)) * this.venta.precio
        }
      }else {
        this.venta.productos_detalle.push({
          producto_id: data[0],
          producto_nombre: data[2],
          cantidad: this.venta.cantidad,
          precio: this.venta.precio,
          subtotal: this.venta.cantidad * this.venta.precio
        });
      }
  
      this.venta.precio_total_venta += Number(this.venta.cantidad) * Number(this.venta.precio);
      this.clearProducto();
    }
  }

  // Metodo para guardar la venta
  guardarVenta() {
    if(this.venta.productos_detalle.length == 0) {
      console.log("no hay productos en la venta");
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer no tienes productos agregados. Intenta nuevamente',
      })
    } else {
      this.venta.id_tienda = "612e8e1f2e4dbd457c7c6751";
      this.ventaService.guardarVenta(this.venta).subscribe(resp => {
        console.log("esta es la respuesta de la venta: ",resp);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Venta almacenada exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
      });
      this.clearCampos();
    }
  }

  // Metodo para guardar la venta en el localstorage
  guardarVentaLocalStorage() {
    if(this.venta.productos_detalle.length == 0) {
      console.log("no hay productos en la venta");
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer no tienes productos agregados. Intenta nuevamente',
      })
    } else {
      this.Util.saveInLocalStorage(ConstantsVflex.Venta.ventaAlmacenada, JSON.stringify(this.venta));
      this.ventasAlmacenadas.push(this.venta);
      this.clearCampos();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Venta creada y guardada exitosamente',
        showConfirmButton: false,
        timer: 1500
      });
      console.log(this.ventasAlmacenadas);
    }
  }

  // Metodo para cancelar la venta
  CancelarVenta() {
    if(this.venta.productos_detalle.length == 0) {
      console.log("no hay productos en la venta");
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer no tienes productos agregados. Intenta nuevamente',
      })
    } else {
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Venta cancelada exitosamente',
        showConfirmButton: false,
        timer: 1500
      })
      this.clearCampos();
    }
  }

  // Metodo para cancelar una venta guardada
  CancelarVentaGuardada(i: any) {
    this.ventasAlmacenadas.forEach(element => {
      this.ventasAlmacenadas.splice(i,1);
      console.log("estas son las ventas que quedan: ",this.ventasAlmacenadas);
    })
  }

  // Metodo para limpiar los campos
  clearCampos() {
    this.venta.cantidad = 0;
    this.venta.precio = 0;
    this.venta.precio_total_venta = 0;
    this.venta.producto = '';
    this.venta.productos_detalle = [];
  }

  // Metodo para limpiar el producto a seleccionar
  clearProducto() {
    this.venta.producto = '';
    this.venta.precio = 0;
    this.venta.cantidad = 0;
  }

  // Eliminar producto de la lista de productos de la venta
  deleteProducto(id: string, i: any) {
    console.log("este es el id que voy a eliminar", id);
    this.venta.productos_detalle.forEach(element => {
      if(element.producto_id == id) {
        this.venta.productos_detalle.splice(i,1);
        this.venta.precio_total_venta = this.venta.precio_total_venta - element.subtotal;
      }
    });

    if(this.venta.productos_detalle.length == 0) {
      this.venta.precio_total_venta = 0;
    }
  }


}
