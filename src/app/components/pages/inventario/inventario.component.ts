import { Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { InventarioService } from '../../../services/inventario.service';
import { MessagesAppService } from '../../../shared/messages-app/messages-app.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { Producto } from '../models/producto.model';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit{

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  productos: MatTableDataSource<Producto>;
  producto: Producto = new Producto();
  closeResult = '';
  loader: boolean = false;
  

  constructor(private inventarioService: InventarioService, public messageService: MessagesAppService, private modalService: NgbModal) {
    this.listarProductos();
   }

  ngOnInit(): void {
 
  }

  //CODIGO PARA CONSUMIR SERVICIO GET DE PRODUCTOS Y GUARDARLOS EN EL ARRAY PARA 
  //MOSTRAR LA DATA EN LA TABLA

  listarProductos() {
    this.loader = true;
    this.inventarioService.listaInventario()
      .subscribe((data:any) => {
        this.productos = new MatTableDataSource<Producto>(data.productos);
        this.loader = false;
        this.productos.paginator = this.paginator;
        this.productos.sort = this.sort;
      });
  }

  //CODIGO PARA GENERAR COLUMNAS Y DATA EN LA TABLA Y FILTRO /////

  displayedColumns: string[] = [this.messageService.messageData.ScreenInventario.campo1tabla, this.messageService.messageData.ScreenInventario.campo2tabla, this.messageService.messageData.ScreenInventario.campo3tabla, this.messageService.messageData.ScreenInventario.campo4tabla, this.messageService.messageData.ScreenInventario.campo5tabla];


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.productos.filter = filterValue.trim().toLowerCase();
  }


  //FUNCIONES POP UP /////


  openInsert(content:any) { 
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  openPut(content:any, producto?: Producto) {
    this.producto = producto; 
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  /////FUNCIONES CRUD PRODUCTOS////

  //// GUARDAR PRODUCTO ////

  guardar(form: NgForm) {

    if(form.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer te falta un campo. Intenta nuevamente',
      })
    }
    
    this.producto.id_tienda = "123456789";

    this.inventarioService.crearProducto(this.producto)
      .subscribe(resp => {
        console.log("se realizo la insersion"+this.producto.bc);
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Producto guardado exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
      })
  }

  //// ELIMINAR PRODUCTO ////

  EliminarProducto(producto: Producto) {
    Swal.fire({
      title: `¿Estas seguro de eliminar el producto: ${producto.nombre}?`,
      showCancelButton: true,
      confirmButtonText: `Eliminar`
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Producto eliminado exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
        this.inventarioService.eliminarProducto(producto.id);
      }
    })
  }

  //// EDITAR PRODUCTO ////

  EditarProducto(form: NgForm) {
    if(form.invalid) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Al parecer te falta un campo. Intenta nuevamente',
      })
    }
    
    this.producto.id_tienda = "123456789";

    Swal.fire({
      title: `¿Estas seguro de editar el producto: ${this.producto.nombre}?`,
      showCancelButton: true,
      confirmButtonText: `Confirmar`
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Producto actualizado exitosamente',
          showConfirmButton: false,
          timer: 1500
        })
        this.inventarioService.actualizarProducto(this.producto.id,this.producto);
      }
    })
  }

}

