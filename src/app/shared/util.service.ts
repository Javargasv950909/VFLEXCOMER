import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  /**Metodo Generico para guardar valores en el local storage */
  saveInLocalStorage(key: string, value:any) {
    localStorage.setItem(key,value);
  }

  /**Metodo Generico para obtener un valor del local storage */
  getLocalStorage(key: string): any {
    return localStorage.getItem(key);
  }

  /**Metodo Generico para remover un valor del local storage */
  removeLocalStorage(key: string): any {
    return localStorage.removeItem(key);
  }

  /**Metodo para confirmar conexion a internet */
  checkNetwork() {
    return navigator.onLine;
  }

}
