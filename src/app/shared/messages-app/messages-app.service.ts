import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessagesAppService {

  messageData: any;
  baseURL: string = './assets/config/text.json';

  constructor(private http: HttpClient) { }

  //Metodo para cargar la configuracion de los mensajes y textos estaticos de la App

  async loadAppMessages() {
    await this.http.get<any>(this.baseURL)
    .toPromise()
    .then((res:any) => {
      this.messageData = res['data'].texts;
    })
    .catch((err: Error) => {
      return err;
    });
  }

}
